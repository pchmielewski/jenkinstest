﻿using JenkinsExample;
using NUnit.Framework;

namespace JenkinsExampleSecondProject
{
    [TestFixture]
    public class ProgrammingLanguagesTest
    {
        [Test]
        public void CheckBadLanguage()
        {
            const ProgrammingLanguagesType type = ProgrammingLanguagesType.UglyJava;

            Assert.AreEqual(type, ProgrammingLanguagesType.UglyJava);
        }

        
#warning Example warning

        /*[Test]
        public void CheckNiceLanguage()
        {
            const ProgrammingLanguagesType type = ProgrammingLanguagesType.UglyJava;

            Assert.AreEqual(type, "Ruby");
        }*/
    }
}
