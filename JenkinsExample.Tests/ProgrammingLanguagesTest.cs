﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace JenkinsExample.Tests
{
    [TestFixture]
    public class ProgrammingLanguagesTest
    {
        [Test]
        public void CheckGoodLanguage()
        {
            //TODO Sth to do
            const ProgrammingLanguagesType type = ProgrammingLanguagesType.CSharp;

            Assert.AreEqual(type, ProgrammingLanguagesType.CSharp);
        }
    }
}
